#include "CoreMinimal.h"
#include "Core\Public\Misc\AutomationTest.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FNPCTalkTest, "Project.Talk.Speak", EAutomationTestFlags::ClientContext | EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter);

bool FNPCTalkTest::RunTest(const FString& Parameters)
{
	TestEqual(TEXT("NPC should says: Hello World!"),"NPC should says: Hello World!",TEXT("NPC should says: Hello World!"));

	return true;
}

//IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPlaceholderTest, "TestGroup.TestSubgroup.Placeholder Test", EATF_GameAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
//
//bool FPlaceholderTest::RunTest(const FString& Parameters)
//{
//	// Make the test pass by returning true, or fail by returning false.
//	return true;
//}
//
//
//IMPLEMENT_SIMPLE_AUTOMATION_TEST(FSetResTest, "Windows.SetResolution", ATF_Game)
//
//bool FSetResTest::RunTest(const FString& Parameters)
//{
//	FString MapName = TEXT("AutomationTest");
//	FEngineAutomationTestUtilities::LoadMap(MapName);
//
//	int32 ResX = GSystemSettings.ResX;
//	int32 ResY = GSystemSettings.ResY;
//	FString RestoreResolutionString = FString::Printf(TEXT("setres %dx%d"), ResX, ResY);
//
//	ADD_LATENT_AUTOMATION_COMMAND(FEngineWaitLatentCommand(2.0f));
//	ADD_LATENT_AUTOMATION_COMMAND(FExecStringLatentCommand(TEXT("setres 640x480")));
//	ADD_LATENT_AUTOMATION_COMMAND(FEngineWaitLatentCommand(2.0f));
//	ADD_LATENT_AUTOMATION_COMMAND(FExecStringLatentCommand(RestoreResolutionString));
//
//	return true;
//}
//
//IMPLEMENT_COMPLEX_AUTOMATION_TEST(FLoadAllMapsInGameTest, "Maps.LoadAllInGame", ATF_Game)
//
//void FLoadAllMapsInGameTest::GetTests(TArray<FString>& OutBeautifiedNames, TArray <FString>& OutTestCommands) const
//{
//	FEngineAutomationTestUtilities Utils;
//	TArray<FString> FileList;
//	FileList = GPackageFileCache->GetPackageFileList();
//
//	// Iterate over all files, adding the ones with the map extension..
//	for (int32 FileIndex = 0; FileIndex < FileList.Num(); FileIndex++)
//	{
//		const FString& Filename = FileList[FileIndex];
//
//		// Disregard filenames that don't have the map extension if we're in MAPSONLY mode.
//		if (FPaths::GetExtension(Filename, true) == FPackageName::GetMapPackageExtension())
//		{
//			if (!Utils.ShouldExcludeDueToPath(Filename))
//			{
//				OutBeautifiedNames.Add(FPaths::GetBaseFilename(Filename));
//				OutTestCommands.Add(Filename);
//			}
//		}
//	}
//}
//
//bool FLoadAllMapsInGameTest::RunTest(const FString& Parameters)
//{
//	FString MapName = Parameters;
//
//	FEngineAutomationTestUtilities::LoadMap(MapName);
//	ADD_LATENT_AUTOMATION_COMMAND(FEnqueuePerformanceCaptureCommands());
//
//	return true;
//}