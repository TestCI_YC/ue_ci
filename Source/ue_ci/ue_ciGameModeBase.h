// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ue_ciGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE_CI_API Aue_ciGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
