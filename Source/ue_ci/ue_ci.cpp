// Copyright Epic Games, Inc. All Rights Reserved.

#include "ue_ci.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ue_ci, "ue_ci" );
