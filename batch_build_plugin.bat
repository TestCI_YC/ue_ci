SET ENGINE_DIR=D:\CKBuild\CKStudio_NoJS\
SET SOURCE_DIR=%CI_PROJECT_DIR%
SET OUTPUT_DIR=F:\AutoBuild\CachePlugins_4.23\
echo CI_PROJECT_DIR:%CI_PROJECT_DIR%
rd /s /q %ENGINE_DIR%Engine\Plugins\EnginePlugins\

::xcopy %OUTPUT_DIR%CKPCGPlugin %ENGINE_DIR%Engine\Plugins\EnginePlugins\CKPCGPlugin\ /s /e 
::xcopy %OUTPUT_DIR%TweenMaker %ENGINE_DIR%Engine\Plugins\EnginePlugins\TweenMaker\ /s /e 
::xcopy %OUTPUT_DIR%CKThirdpartyPlugin %ENGINE_DIR%Engine\Plugins\EnginePlugins\CKThirdpartyPlugin\ /s /e 

echo 'start build'
REM for /f "delims=" %%i in ('') do (set a=%%i) 
call %ENGINE_DIR%Engine\Build\BatchFiles\RunUAT.bat BuildPlugin -Rocket -Plugin=%SOURCE_DIR%\CKTestAutoPack.uplugin -TargetPlatforms=Win64 -Package=%OUTPUT_DIR%CKTestAutoPack
if %errorlevel% neq 0 exit /b %errorlevel%
echo 'end build'

rd /s /q %ENGINE_DIR%Engine\Plugins\EnginePlugins\

    
echo 'build success'